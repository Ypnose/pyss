yss - [y]pnose's [s]ystem [s]ummary
===================================

A script to display some information about the current system.

## Why?

It was painful to launch many commands to fetch system properties. I
also wanted to have a visual representation of the memory, including
values.  
I did not want to install external tools like ```htop``` on my minimal
and critical embedded servers.

## Usage

```awk(1)``` must be installed on the system. If not, an error will be
returned. It was written using ```nawk(1)```.

	$ sh yss
	HOST:   opinel
	DATE:   2019-01-06 16:20:11
	OS:     Linux
	KERNEL: 4.14.91_1
	UPTIME: 1 day
	LOAD:   0.51 0.67 0.98
	RAM:    [**oooooooo          ] 1.57(*)/6.95(o)/15.64 GiB [10%(*)/44%(o)]
	SWAP:   [                    ] 0.00/8.00 GiB [0%]

For the **```RAM```** line, each character represents 5% of the total.
If the total is greater than 4096 MiB, the values are converted to GiB.
**```*```** means used memory and **```o```** means buffers and cached
memory.

I tend to launch this script after a successful login, using ```${HOME}/.profile```.

As always with my projects, it is a good idea to read the sources.

## Notes

Memory line (**```RAM```**) currently works on Linux ONLY.

For consistensy with [procps-ng](https://gitlab.com/procps-ng/procps)
(```v3.3.12```) and therefore tools like ```free(1)```, ```vmstat(1)```
or ```top(1)```, I read the sources and did the same calculations.

Prior to ```procps-ng v3.3.10```, the values will not be the same (if
you want to compare). ```procps-ng``` was including buffers and cached
memory in used memory. That's why it is different.

## Author

Ypnose - http://ywstd.fr/

## License

BSD 3-Clause License. Check LICENSE.
